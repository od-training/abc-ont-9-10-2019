import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video } from './dashboard/types';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VideoLoaderService {

  constructor(private http: HttpClient) { }

  getVideos() {
    return this.http.get<Video[]>('https://api.angularbootcamp.com/videos').pipe(
      map(videos => videos.map(video => ({
        ...video,
        title: video.title.toUpperCase(),
        })
      )),
    );


  }
}

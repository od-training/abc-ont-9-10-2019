import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Video, VIDEO_ID_QUERY_PARAM, QueryParamIds } from '../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Input() videoList: Video[] | undefined;

  selectedId = '';

  constructor(private router: Router) {}

  ngOnInit() {}

  selectVideo(video: Video) {
    this.selectedId = video.id;
    this.router.navigate([], {
      queryParams: {
        [QueryParamIds.VideoId]: this.selectedId
      },
      queryParamsHandling: 'merge'
    });
  }
}

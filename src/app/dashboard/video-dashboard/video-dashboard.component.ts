import { Component, OnInit } from '@angular/core';
import { Video, VIDEO_ID_QUERY_PARAM, QueryParamIds } from '../types';
import { VideoLoaderService } from 'src/app/video-loader.service';
import { Observable, combineLatest, BehaviorSubject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, tap, switchMap, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  videos: Observable<Video[]>;
  selectedVideo: Observable<Video | undefined>;
  fetchVideos = new BehaviorSubject(undefined);

  constructor(vls: VideoLoaderService, route: ActivatedRoute) {
    this.videos = this.fetchVideos.pipe(
      switchMap(() => vls.getVideos())
    );

    const videoId: Observable<string> = route.paramMap.pipe(
      map(params => params.get(QueryParamIds.VideoId) || '')
    );

    this.selectedVideo = combineLatest(this.videos, videoId).pipe(
      tap((streams) => console.log(streams)),
      map(([videoList, selectedId]) => {
        return videoList.find(v => v.id === selectedId);
      })
    );
  }

  ngOnInit() {}

  refreshVideos() {
    this.fetchVideos.next(undefined);
  }

  setFilter(value: string) {
    console.log(value);
  }
}

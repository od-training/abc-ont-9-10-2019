import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnInit, OnDestroy {
  @Output() filterChanged = new EventEmitter<string>();
  videoFilter: FormControl;
  destroy = new Subject();

  constructor() {
    this.videoFilter = new FormControl('');

    const fg = new FormGroup({
      filter: new FormControl()
    });

    const filterControl = fg.get('filter');

    if (filterControl) {
      filterControl.valueChanges
        .pipe(takeUntil(this.destroy))
        .subscribe(value => this.filterChanged.emit(value));
    }

    this.videoFilter.valueChanges
      .pipe(takeUntil(this.destroy))
      .subscribe(value => this.filterChanged.emit(value));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.destroy.next('hey');
    this.destroy.complete();
  }

}

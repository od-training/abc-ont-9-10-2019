export const VIDEO_ID_QUERY_PARAM = 'videoId';

export enum QueryParamIds {
  VideoId = 'videoId'
}

export interface Video {
  title: string;
  author: string;
  id: string;
  viewDetails: ViewDetail[];
}

interface ViewDetail {
  age: number;
  region: string;
  date: string;
}
